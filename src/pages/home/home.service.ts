import { environment } from './../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './../../environments/base.service';
import { Injectable } from '@angular/core';

@Injectable()
export class HomeService extends BaseService{

    constructor(public http: HttpClient){super(http)}

    getNomeDeputado(nomeDeputado:any){
        let url = environment.urlApi + "?nome=" + nomeDeputado;
        return this.get(url);
    }

    getDeputados(pagina){
        let url = environment.urlApi + "?pagina=" + pagina + "&itens=15";
        return this.get(url);
    }

    getDespesas(idDeputado:any){
        let url = environment.urlApi + idDeputado + "/despesas";
        return this.get(url);
    }


}