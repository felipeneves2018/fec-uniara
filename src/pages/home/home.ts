import { DespesasPage } from './../despesas/despesas';
import { Despesas } from './../../app/API_CLASS/depesas';
import { Deputado } from './../../app/API_CLASS/deputado';
import { HomeService } from './home.service';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  deputado = new Array<Deputado>();
  despesas = new Array<Despesas>();
  nomeDeputado: string;
  pagina: any = 1;

  constructor(public navCtrl?: NavController,
              public homeService?: HomeService){
  }

  buscarNomeDeputado(){
    if (this.nomeDeputado != ""){
      this.homeService.getNomeDeputado(this.nomeDeputado)
      .subscribe(data =>{
          this.calbackSucessoBuscarDeputado(data);
          this.nomeDeputado = "";
      },erro =>{
          this.calbackErroBuscarDeputado();
      });
    }else{
      alert("Deputado não encontrado ;(");
    }
    
  }

  buscarDeputados(){
    let pagina = this.pagina
    this.homeService.getDeputados(pagina)
    .subscribe((data) =>{
        this.calbackSucessoBuscarDeputado(data);
    },erro =>{
        this.calbackErroBuscarDeputado();
    });
  }

  despesasDeputado(idDeputado : any){
    this.homeService.getDespesas(idDeputado)
    .subscribe((data) =>{
      this.calbackSucessoBuscarDespesas(data);
    },erro =>{
      this.calbackErroBuscarDespesas();
    });
  }

  calbackSucessoBuscarDeputado(data: any){
    this.deputado = data.dados;
  }

  calbackSucessoBuscarDespesas(data: any){
    this.despesas = data.dados;
    this.navCtrl.push("DespesasPage",{
      data: this.despesas
    })
  }

  calbackErroBuscarDeputado(){
    alert("Deputado não encontrado! ;(");
  }

  calbackErroBuscarDespesas(){
    alert("Despesas do deputado não encontrado");
  }

  removerDeputado(){
    this.deputado = null
  }
}
