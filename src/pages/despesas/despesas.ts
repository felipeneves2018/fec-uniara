import { HomePage } from './../home/home';
import { Despesas } from './../../app/API_CLASS/depesas';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DespesasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-despesas',
  templateUrl: 'despesas.html',
})
export class DespesasPage {

  despesas: any

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.despesas = this.navParams.get('data')
  }

}
