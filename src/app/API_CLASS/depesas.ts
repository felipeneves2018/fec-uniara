export class Despesas {

    constructor(public ano?: number,
                public mes?: number,
                public nomeFornecedor?: string,
                public tipoDocumento?: string,
                public valorLiquido?: number){}
}