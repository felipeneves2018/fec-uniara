export class Deputado{

    constructor(public id?: number,
                public idLegislatura?: number,
                public nome?: string,
                public siglaPartido?: string,
                public siglaUf?: string,
                public uri?: string,
                public uriPartido?: string,
                public uriFoto?: string){}

}