import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map'
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';

export class BaseService{
	constructor(public http: HttpClient){}

	get(url: string){
		return this
				.http
				.get(url)
				.map((response: any) => {
                	return response;
            	});
	}

	setParametro(str: string, chave: string, valor: any) : string{
		return str.replace("{"+chave+"}", valor);
	}
}